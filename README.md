Aplicația se găsește accesând linkul: https://gitlab.upt.ro/paul.herman/herman_paul_licenta

Pentru a putea rula această aplicație, orice utilizator va avea nevoie de un browser web instalat pe mașina de lucru.
În al doilea rând, înainte de rularea aplicației, utilizatorul va trebui să instaleze pe mașina sa de lucru limbajul python împreună cu toate bibliotecile aferente folosite în aplicație.
Pentru a instala limbajul python utilizatorul poate să viziteze pagina web oficială a python folosind link-ul “https://www.python.org/downloads/”. Unele biblioteci folosite în proiect vor fi instalate implicit, o data cu instalarea limbajului python însă altele necistă o instalare separată. Bibliotecile folosite în proiect care necesită o instalare separată sunt următoarele:
-	Streamlit – folosind comanda “pip install streamlit”
-	Matplotlib – folosind comanda “pip install matplotlib”
-	Seaborn – folosind comanda “pip install seaborn”
-	Scikit-learn – folosind comanda “pip install scikit-learn”
Pentru a putea rula aplicația utilizatorul va avea nevoie să dețină toate fișierele “.py” care aparțin aplicației și să le ruleze deschizând command prompt și rulând comanda “streamlit run 1_Home.py”. Odată rulată aceasta comandă se va deschide în browserul folosit implicit pagina de home a aplicației de unde utilizatorul poate începe să folosească întreaga aplicație.

Pentru a putea rula aplicația fără errori se va păstra exact structura găsită pe gitlab.upt.

În pagina principală, home se va încărca de test fisierul "2023.03.27_-_TSR.csv".
Pentru a face o predicție pe un set nou de date, pe pagina "Prediction-Algorithm" se va încărca fisierul "2023.03.29_-_TSR.csv".
