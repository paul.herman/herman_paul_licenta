import pandas as pd
import streamlit as st
import os
from script import function

st.set_page_config(page_title="Dashboard", page_icon=":bar_chart:", layout="wide")

uploaded_file = st.file_uploader("Choose a CSV file", type="csv")

if uploaded_file is not None:
    with open("temp.csv", "wb") as file:
        file.write(uploaded_file.getbuffer())

    st.header("Processing Data")
    function("temp.csv")

    processed_data = pd.read_csv("Read_Data/temp.csv")

    st.header("Processed Data")
    st.write(processed_data)

    with open("Read_Data/temp.csv", "rb") as file:
        file_content = file.read()

    st.markdown(
        f'<div style="display: flex; justify-content: center;">'
        f'<a href="data:file/csv;base64,{file_content.decode()}" download="processed_data.csv">'
        f'<button class="download-button" style="font-size: 16px;">Download Processed File</button>'
        f'</a>'
        f'</div>',
        unsafe_allow_html=True
    )

    os.remove("temp.csv")

