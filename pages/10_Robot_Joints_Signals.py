import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt

st.set_page_config(page_title="Robot-Joints Signals")

processed_data = pd.read_csv("Read_Data/temp.csv")

columns = processed_data.columns.tolist()

st.title("Data Plotting")
st.header("Select Column for Plotting")

column2 = st.selectbox("Select Column for Plotting", columns[110:117])

start_line, end_line = st.slider("Select Range of Lines", min_value=2, max_value=len(processed_data), value=(1, 500))

selected_values = processed_data[start_line-1:end_line][column2]

value_counts = selected_values.value_counts()

top_values = value_counts.head(4)

fig, ax = plt.subplots(figsize=(8, 6))
ax.pie(top_values, labels=top_values.index, autopct='%1.1f%%', startangle=90)
ax.axis('equal')
ax.set_title(f"Data Distribution of {column2}", fontsize=14)
st.pyplot(fig)
