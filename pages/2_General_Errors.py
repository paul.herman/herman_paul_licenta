import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt

st.set_page_config(page_title="General Errors")

processed_data = pd.read_csv("Read_Data/temp.csv")

columns = processed_data.columns.tolist()

st.title("General Errors")
st.header("Select Column for Plotting")
column2 = st.selectbox("Select Column for Plotting", columns[1:19])

pie_start_line, pie_end_line = st.slider("Range of Lines", min_value=2, max_value=len(processed_data), value=(1, 500))
pie_selected_data = processed_data[pie_start_line-1:pie_end_line]

value_counts = pie_selected_data[column2].value_counts()
fig, ax = plt.subplots(figsize=(8, 6))
ax.pie(value_counts, labels=value_counts.index, autopct='%1.1f%%', startangle=90)
ax.axis('equal')
ax.set_title(f"Frequency of Values in {column2}", fontsize=14)
st.pyplot(fig)

st.write("Frequency of Each Entry:")
for entry, frequency in value_counts.items():
    st.write(f"{entry} - {frequency} times")


start_line, end_line = st.slider("Select Range of Lines for Line Plot", min_value=2, max_value=len(processed_data), value=(1, 500))
selected_data = processed_data[start_line-1:end_line]

fig, ax = plt.subplots(figsize=(8, 6))
ax.plot(selected_data[columns[0]], selected_data[column2], color='blue', linewidth=2, marker='o', markersize=4)
ax.set_xlabel(columns[0], fontsize=12)
ax.set_ylabel(column2, fontsize=12)
ax.set_title(f"Plot of {columns[0]} vs {column2}", fontsize=14)
ax.grid(True, linestyle='--', alpha=0.5)
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
st.pyplot(fig)

