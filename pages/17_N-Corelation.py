import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import itertools

st.set_page_config(page_title="N-Signal Correlation")

processed_data = pd.read_csv("Read_Data/temp.csv")

columns = processed_data.columns.tolist()

st.title("Signal Correlation")
st.header("Select Signals for Correlation")

num_signals = st.number_input("Enter the number of signals to select", min_value=2, max_value=len(columns)-1, value=2, step=1)

selected_signals = []
for i in range(num_signals):
    signal = st.selectbox(f"Select Signal {i+1}", columns[1:])
    selected_signals.append(signal)

correlation_matrix = processed_data[selected_signals].corr()

labels = [f"Signal {i+1}" for i in range(num_signals)]
correlation_matrix.columns = labels
correlation_matrix.index = labels

mask = np.triu(np.ones_like(correlation_matrix, dtype=bool))

fig, ax = plt.subplots(figsize=(8, 6))
heatmap = sns.heatmap(correlation_matrix, mask=mask, annot=True, cmap="coolwarm", cbar=True, ax=ax)
ax.set_title("Signal Correlation", fontsize=14)
ax.set_xticklabels(labels, rotation=45)
ax.set_yticklabels(labels)
st.pyplot(fig)

button_label = "Show Interpretation"
interpretation_visible = st.button(button_label, key="interpretation_button")

if interpretation_visible:
    st.write("Interpretation of Signal Correlation:")
    st.write("- The correlation coefficient ranges from -1 to 1.")
    st.write("- A value close to 1 indicates a strong positive correlation.")
    st.write("- A value close to -1 indicates a strong negative correlation.")
    st.write("- A value close to 0 indicates a weak or no correlation.")
    st.button("Hide Interpretation", key="hide_interpretation_button")



