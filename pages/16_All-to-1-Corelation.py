import pandas as pd
import streamlit as st
import itertools

st.set_page_config(page_title="All to 1 Signal Correlation")

processed_data = pd.read_csv("Read_Data/temp.csv")

columns = processed_data.columns.tolist()

st.title("All Signal Correlation")

first_89_columns = processed_data.columns[1:89]
rest_columns = processed_data.columns[89:]

selected_column = st.selectbox("Select Column", first_89_columns)

correlations = []

for signal1, signal2 in itertools.product([selected_column], rest_columns):
    correlation = processed_data[signal1].corr(processed_data[signal2], method='pearson')
    if pd.notna(correlation) and abs(correlation) >= 0.01:
        correlations.append((signal1, signal2, correlation))

correlations.sort(key=lambda x: (x[2] >= 0, abs(x[2]), -x[2]), reverse=True)

st.header(f"Correlations involving {selected_column}")
for i, (signal1, signal2, correlation) in enumerate(correlations):
    st.write(f"Correlation {i+1}: {signal1} vs {signal2}: {correlation:.2f}")



