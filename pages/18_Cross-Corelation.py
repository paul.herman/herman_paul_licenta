import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt
import seaborn as sns

st.set_page_config(page_title="Cross-Correlation Analysis")
processed_data = pd.read_csv("Read_Data/temp.csv")

columns = processed_data.columns.tolist()

st.title("Cross-Correlation Analysis")
st.header("Select Signals for Cross-Correlation")

signal1 = st.selectbox("Select First Signal", columns[1:])

signal2 = st.selectbox("Select Second Signal", columns[1:])

cross_correlation = processed_data[signal1].corr(processed_data[signal2])

fig, ax = plt.subplots(figsize=(8, 6))
cross_corr_plot = sns.lineplot(data=processed_data, x=signal1, y=signal2, ax=ax)
ax.set_title("Cross-Correlation", fontsize=14)
st.pyplot(fig)

button_label = "Show Interpretation"
interpretation_visible = st.button(button_label, key="interpretation_button")

if interpretation_visible:
    st.write("Interpretation of Cross-Correlation Results:")
    st.write("- Time Lag: The x-axis represents the time lag or delay between the two signals being cross-correlated.")
    st.write("- Cross-Correlation Value: The y-axis represents the cross-correlation value between the signals at each time lag.")
    st.write("- Peak Values: Higher peak values indicate stronger correlation between the signals.")
    st.write("- Peak Location: The location of the peak indicates the optimal time lag at which the signals align most closely.")
    st.write("- Symmetry: Cross-correlation plots are symmetric around the time lag axis.")
    st.write("Use these insights to understand the relationship and temporal patterns between the selected signals.")
    st.button("Hide Interpretation", key="hide_interpretation_button")

