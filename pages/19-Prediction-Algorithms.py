import pandas as pd
import streamlit as st
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import classification_report

st.set_page_config(page_title="Machine Learning")

df = pd.read_csv("Read_Data/temp.csv")

new_df = df.drop(df.columns[0], axis=1)

columns = new_df.columns.tolist()

valid_columns = []
for column in columns:
    unique_values = new_df[column].unique()
    if 0 in unique_values and 1 in unique_values:
        valid_columns.append(column)

selected_column = st.selectbox("Select a Column", valid_columns)
selected_df = new_df.drop(new_df.columns[:88].difference([selected_column]), axis=1)

selected_df = selected_df[[col for col in selected_df.columns if col != selected_column] + [selected_column]]

train, valid = train_test_split(selected_df, test_size=0.3, random_state=42)

st.subheader("Train Set")
st.write(selected_df)

st.subheader("Upload Second File")
uploaded_file = st.file_uploader("Choose a second CSV file", type="csv")

if uploaded_file is not None:
    second_df = pd.read_csv(uploaded_file)

    second_new_df = second_df.drop(second_df.columns[0], axis=1)

    second_selected_df = second_new_df.drop(second_new_df.columns[:88].difference([selected_column]), axis=1)

    second_selected_df = second_selected_df[
        [col for col in second_selected_df.columns if col != selected_column] + [selected_column]]

    scaler = StandardScaler()
    X_test_second = scaler.fit_transform(second_selected_df[second_selected_df.columns[:-1]].values)

    model_selection = st.selectbox("Select Model", ["KNN", "Gaussian Naive Bayes", "Logistic Regression", "SVM"])

    if model_selection:
        X = selected_df[selected_df.columns[:-1]].values
        y = selected_df[selected_df.columns[-1]].values
        scaler = StandardScaler()
        X = scaler.fit_transform(X)
        X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2, random_state=42)

        if model_selection == "KNN":
            knn_model = KNeighborsClassifier(n_neighbors=5)
            knn_model.fit(X_train, y_train)
            y_pred = knn_model.predict(X_valid)
            st.write("KNN Classification Report")
            st.text(classification_report(y_valid, y_pred))

        elif model_selection == "Gaussian Naive Bayes":
            nb_model = GaussianNB()
            nb_model.fit(X_train, y_train)
            y_pred = nb_model.predict(X_valid)
            st.write("Gaussian Naive Bayes Classification Report")
            st.text(classification_report(y_valid, y_pred))

        elif model_selection == "Logistic Regression":
            unique_classes = np.unique(y_train)
            if len(unique_classes) < 2:
                st.write("Logistic Regression requires data with at least two classes.")
            else:
                lg_model = LogisticRegression()
                lg_model.fit(X_train, y_train)
                y_pred = lg_model.predict(X_valid)
                st.write("Logistic Regression Classification Report")
                st.text(classification_report(y_valid, y_pred))

        elif model_selection == "SVM":
            svm_model = SVC()
            svm_model.fit(X_train, y_train)
            y_pred = svm_model.predict(X_valid)
            st.write("SVM Classification Report")
            st.text(classification_report(y_valid, y_pred))

        if model_selection == "KNN":
            y_pred_second = knn_model.predict(X_test_second)
        elif model_selection == "Gaussian Naive Bayes":
            y_pred_second = nb_model.predict(X_test_second)
        elif model_selection == "Logistic Regression":
            y_pred_second = lg_model.predict(X_test_second)
        elif model_selection == "SVM":
            y_pred_second = svm_model.predict(X_test_second)

        st.subheader("Predictions for Second File")
        predictions_second = pd.DataFrame({
            "Data": second_selected_df[selected_column].values,
            "Prediction": y_pred_second
        })

        # Add actual values from the second file
        predictions_second['Actual'] = second_selected_df[selected_column].values

        # Calculate match percentage
        match_percentage = (predictions_second['Prediction'] == predictions_second['Actual']).mean() * 100
        st.subheader(f"Match Percentage: {match_percentage:.2f}%")

        st.write(predictions_second)

