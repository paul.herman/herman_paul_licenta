import os
import sys
import pandas

def function(filename):
		df1=pandas.read_csv(filename, delimiter=",", header = 0, skipinitialspace=True)
		df2=pandas.DataFrame()

		sensors=df1["Tag"].astype(str)
		sensors_set=set(sensors)
		sensors_list=sorted(list(sensors_set))
		
		d={}
		d["time"]=df1["_time"]
		i=0
		for col in sensors_list:
			d[col]=df1["Value"][df1["Tag"]==col]
			i+=1
		
		df2=pandas.DataFrame(d)

		for col in sensors_list:
			df2[col] = df2[col].fillna(0)

		df3=df2.groupby(by=["time"],as_index=False).sum()
		
		df3.to_csv("Read_Data/"+filename,index=False)